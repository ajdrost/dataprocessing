#configfile config.yaml
genome: data/A
ext : .fa
samples:
    A : /data/A.bam
    B : /data/B.bam
    C : /data/C.bam

#-----------------------------------------------------<
# YAML tranform param file
#-----------------------------------------------------<
FLOWCELL: ULYSSE HOMER HADES

# kan je deze cijfers zo dr achter zetten?
#----------------------<
ULYSSE_LANES: 5 6
HOMER_LANES: 3 4
HADES_LANES: 1 2
#----------------------<

TECHNOLOGY: ILLUMINA
REFGEN: Gmax_275_v2.0.fa
ADAP: AGATCGGAA
READLEN: 50
BWAPAR: 2
BWATHR: 4
NBCOR: 8
LOGFILE: logfile_fastgbs.log
BAMLIST: List_bam
MINREADS: 2
LOGPLAT: FastGBS_platypus_log.txt
OUTPLAT: FastGBS_platypus

fastq_files: data/ (en dan + *.fastq??)
makeBarcode: programs/makeBarcodeSabre

